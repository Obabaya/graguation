﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayState : ISceneState
{
    public PlayState(SceneStateController Controller) : base(Controller)
    {
        StateName = "PlayState";
    }

    // 開始
    public override void StateBegin()
    {
        // 取得開始按鈕
        Button tmpBtn = UITool.GetUIComponent<Button>("ReturnBtn");
        Debug.Log(tmpBtn);
        if (tmpBtn != null)
            tmpBtn.onClick.AddListener(() => OnStartGameBtnClick(tmpBtn));
    }

    // 開始戰鬥
    private void OnStartGameBtnClick(Button theButton)
    {
        Debug.Log("OnStartBtnClick:" + theButton.gameObject.name);
        m_Controller.SetState(new MainMenuState(m_Controller), "MainMenuScene");
    }
}
