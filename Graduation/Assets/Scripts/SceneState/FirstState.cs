﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//開場狀態
public class FirstState : ISceneState
{
    public FirstState(SceneStateController Controller) : base(Controller)
    {
        StateName = "FirstState";
    }

    // 開始
    public override void StateBegin()
    {
        // 可在此進行遊戲資料載入及初始...等
    }

    // 更新
    public override void StateUpdate()
    {
        // 更換為
        m_Controller.SetState(new MainMenuState(m_Controller), "MainMenuScene");
    }
}
