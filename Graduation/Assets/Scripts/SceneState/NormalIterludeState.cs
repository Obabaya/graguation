﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NormalIterludeState : IIterludeState
{
    AsyncOperation AsOp = null;
    private Slider m_ProgressBar = null;
    private Text m_PregressText = null;

    public NormalIterludeState(SceneStateController Controller):base(Controller)
    {
        StateName = "NormalIterludeState";
    }

    // Start is called before the first frame update
    public override void StateBegin()
    {
        Debug.Log("IterludeBegin");
        m_ProgressBar = UITool.GetUIComponent<Slider>("ProgressBar");
        m_PregressText = UITool.GetUIComponent<Text>("ProgressText");
        CoroutineRunner.RunCoroutine(LoadSceneAsync());
        //StartCoroutine(LoadSceneAsync());
    }

    private IEnumerator LoadSceneAsync()
    {
        int toProgress = 0;
        int displayProgress = 0;
        AsOp = SceneManager.LoadSceneAsync(m_NextScene);
        AsOp.allowSceneActivation = false;

        while (AsOp.progress < 0.9f)
        {
            toProgress = (int)AsOp.progress * 100;
            while(displayProgress < toProgress)
            {
                ++displayProgress;
                SetLoadingPercentage(displayProgress);
                yield return new WaitForEndOfFrame();
            }
            Debug.Log(AsOp.progress);
            //防止AsOp無變動時進入死迴圈
            yield return null;
        }

        Debug.Log(AsOp.progress);

        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            SetLoadingPercentage(displayProgress);
            yield return new WaitForEndOfFrame();
        }
        m_Controller.SetState(m_NextState, "");
        AsOp.allowSceneActivation = true;
    }

    private void SetLoadingPercentage(int value)
    {
        m_ProgressBar.value = value;
        m_PregressText.text = value + "%";
    }

}
