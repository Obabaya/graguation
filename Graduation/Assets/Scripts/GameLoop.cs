﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoop : MonoBehaviour
{
    // 場景狀態
    SceneStateController m_SceneStateController = new SceneStateController();

    // 
    void Awake()
    {
        // 切換場景不會被刪除
        DontDestroyOnLoad(this.gameObject);

        // 亂數種子
        UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
    }

    // Use this for initialization
    void Start()
    {
        // 設定起始的場景
        m_SceneStateController.SetState(new FirstState(m_SceneStateController), "");
    }

    // Update is called once per frame
    void Update()
    {
        m_SceneStateController.StateUpdate();
    }
}
